import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        //HashMap games
        HashMap<String,Integer> games=new HashMap<>();
        games.put("Mario Odyssey",50);
        games.put("Super Smash Bros. Ultimate",20);
        games.put("Luigi's Mansion 3",15);
        games.put("Pokemon Sword",30);
        games.put("Pokemon Shield",100);

        //topGames arrayList
        ArrayList<String> topGames=new ArrayList<>();

        //loop over games HashMap
        games.forEach((key,value)->{
            System.out.println(key + " has "+ value +" stocks left.");
           /* if (value<=30){
                topGames.add(key);
                System.out.println(key +" has been added to top games list!");
            }
            */
        });

        //adding to topGames arrayList
        games.forEach((key,value)->{
            if (value<=30){
                topGames.add(key);
                System.out.println(key +" has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games: ");
        System.out.println(topGames);


    }
}